#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 10:07:08 2018

@author: spirit678
"""
from BioRanges.lightweight import Range, Ranges, SeqRange, SeqRanges 
f0=open("./Alu_out.bed", "r")
out=open("./lnc_vs_Alu.bed", "w")
out2=open("./lnc_vs_Alu_nonexonic.bed", "w")

for line in f0:
    c=0
    j=0
    line1 = line.strip().split('\t')
    cromosome=line1[0]
    start=int(line1[1])
    end=line1[2]
    num=line1[9]
    strn=line1[5]
    Gidx=line1[3]
    exonSt=list(line1[10].split(","))
    exonEn=list(line1[11].split(","))
    AluSt=line1[13]
    AluEn=line1[14]
    for x, y in zip(exonSt, exonEn):
        c+=1
        exon_s = int(start) + int(y) + 1
        exon_e = int(start) + int(y) + int(x)
        xx = Range(int(exon_s),int(exon_e))
        yy = Range(int(AluSt), int(AluEn))
        inter = yy.overlaps(xx)
        if yy.overlaps(xx) == True:
            out.write(line)
            j = 1
        if yy.overlaps(xx) == False:
            if c == int(num):
                if j == 0:
                    print(num, c, j)
                    out2.write(line)
                else:
                    pass
            else:
                pass
                #print(num, c, j)
        else:
            pass
        

out.close()
out2.close() 
f0.close()          

"""  
    name=str(line1[3])
	chrom=str(line1[0])
	if len(chrom) < 6:
		if name.startswith("Alu"):
			write = line
			print(line)
			out.write(write)
		else:
			print("I am not Alu")
#	else:
		#print("HoHoHo I am patch!")
Exon_range
for i in  exonSt.strip().split(',')
St= exonSt
Alu_range
"""
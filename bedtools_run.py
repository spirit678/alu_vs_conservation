#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 17:18:37 2018

@author: spirit678
"""
import pandas as pd
import csv
import subprocess
from BioRanges.lightweight import Range, Ranges, SeqRange, SeqRanges
from collections import defaultdict

IDx = pd.read_csv("./human/counterparts.csv")

mainS =IDx.copy()
mainY =IDx.copy()
mainJ =IDx.copy()


species_list = ["Chimp","Bonobo","Gorilla","Orangutan","Rhesus","MacFas", "Atys","Baboon","GreenMonkey","Marmoset","Lemur"]
save_path = "./Alu_count/"
for q in species_list:
    path = "./primates/done/" + q + "/"
    bedtools = "bedtools intersect -F 1.00 -wo -a " + str(path + q +".bed") + " -b " + str(path + q +"_Alu.bed") +  " > " + str( save_path + q + "_unsorted.bed")
    ps = subprocess.Popen(bedtools,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    res = ps.communicate()
    print(res)
    AluS = defaultdict(list)
    AluJ = defaultdict(list)
    AluY = defaultdict(list)
    with open(str( save_path + q + "_unsorted.bed")) as tsvfile:
        parse = csv.reader(tsvfile, delimiter="\t")
        for row in parse:
            cromosome=row[0]
            start=int(row[1])
            end=row[2]
            num=row[9]
            strn=row[5]
            Gidx=row[3]
            exonSt=list(row[10].split(","))
            exonEn=list(row[11].split(","))
            AluSt=row[13]
            AluEn=row[14]
            trans_id = row[3]
            Alu_id = row[15]
            for x, y in zip(exonSt, exonEn):
                exon_s = int(start) + int(y) + 1
                exon_e = int(start) + int(y) + int(x)
                xx = Range(int(exon_s),int(exon_e))
                yy = Range(int(AluSt), int(AluEn))
                inter = yy.overlaps(xx)
                if yy.overlaps(xx) == True:
                    #out.write(line)
                    #print(trans_id, Alu_id)
                    if Alu_id.startswith("AluS"):
                        AluS[trans_id].append(1)
                    elif Alu_id.startswith("AluJ"):
                        AluJ[trans_id].append("AluJ")
                    elif Alu_id.startswith("AluY"):
                        AluY[trans_id].append("AluY")
                else:
                    pass
    AluS = pd.Series({key: len(value) for key, value in AluS.items()}).to_frame().reset_index()
    AluS.columns = [str(q),str(q + "_Alu")]
    mainS = pd.merge(mainS, AluS, on=str(q), how='left')
    AluY = pd.Series({key: len(value) for key, value in AluY.items()}).to_frame().reset_index()
    AluY.columns = [str(q),str(q + "_Alu")]
    mainY = pd.merge(mainY, AluY, on=str(q), how='left')
    AluJ = pd.Series({key: len(value) for key, value in AluJ.items()}).to_frame().reset_index()
    AluJ.columns = [str(q),str(q + "_Alu")]
    mainJ = pd.merge(mainJ, AluJ, on=str(q), how='left')
    
Human_Alu = pd.read_csv("./human/Alu.csv")
H_AluS = Human_Alu[["Transcript", "AluS"]].copy()
H_AluS.columns = ["lnc", "Human_Alu"]
H_AluY = Human_Alu[["Transcript", "AluY"]].copy()
H_AluY.columns = ["lnc", "Human_Alu"]
H_AluJ = Human_Alu[["Transcript", "AluJ"]].copy()
H_AluJ.columns = ["lnc", "Human_Alu"]
mainS = pd.merge(mainS, H_AluS, on="lnc", how='left')
mainY = pd.merge(mainY, H_AluY, on="lnc", how='left')
mainJ = pd.merge(mainJ, H_AluJ, on="lnc", how='left')

mainS.to_csv(str(save_path + "AluS_united.tsv"), sep='\t', encoding='utf-8')
mainY.to_csv(str(save_path + "AluY_united.tsv"), sep='\t', encoding='utf-8')
mainJ.to_csv(str(save_path + "AluJ_united.tsv"), sep='\t', encoding='utf-8')

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 15:00:20 2018

@author: spirit678
"""
import pandas as pd
import csv
methods = ["clustalo","needle"]
for i in methods:
    species_list = ["Chimp","Bonobo","Gorilla","Orangutan","Rhesus","MacFas","Atys",
                    "Baboon","GreenMonkey","Marmoset","Lemur"]
    df_load = pd.read_table("./human/counterparts.csv", sep=",")
    df_main = df_load[["lnc","ensembl_transcript_id","Length"]].copy()
    df_main.columns = ["Transcript","ensembl_transcript_id","Length"]
    
    for x in species_list:
        file_path = "./results/" + str(i) + "/" + str(x) + ".tsv"
        df = pd.read_table(file_path, header=None)
        df.columns = ['Transcript','conterpart','lenghth', str(x)]
        df = df[["Transcript", str(x)]]
        df_main= pd.merge(df_main, df, on='Transcript', how='left')
    df_main.to_csv(str('./results/output_' + str(i) + '.csv'), quoting=csv.QUOTE_NONE)


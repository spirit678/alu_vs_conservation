#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  1 13:50:20 2018

@author: spirit678
"""

from Bio import SeqIO
import csv
import subprocess



def align(primate):
    print(primate)
    hum_primate_dict = {}
    summary= open(str(primate + "_needle.tsv"), 'w')
    
    count_na = 0
    count_ort = 0
    c=0
    with open('./human/counterparts.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                hum_primate_dict[row["lnc"]] = row[str(primate)]
    
    for key, value in hum_primate_dict.items():
        if value != "NA":
            print(key, value)
            count_ort +=1
            fout = str('./temp/' + key + ".fa")
            fout2 = str('./temp/' + value + ".fa")
            fin = open('./human/transcripts.fa', 'r')
            f2in = open(str('./primates/done/' + primate +"/" + primate + ".fa"))
            for record in SeqIO.parse(fin,'fasta'):
                    c+=1
                    if record.id == key:
                        seqLen = len(record)
                        SeqIO.write(record, fout, "fasta")
                    else:
                        pass
            for record in SeqIO.parse(f2in,'fasta'):
                    c+=1
                    if record.id == value:
                        SeqIO.write(record, fout2, "fasta")
                    else:
                        pass
            fin.close()
            f2in.close()
            cmd = str("needle -asequence " + fout + " -bsequence " + fout2 + "-gapopen 10.0 -gapextend 0.5 -outfile ./temp/result.txt")
            ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
            res = ps.communicate()[0]
            output_ide = subprocess.check_output(['grep "Identity" ./temp/result.txt'])
            #output_sim = subprocess.check_output(['grep "Similarity" result.txt'])
            #output_gap = subprocess.check_output(['grep "Gaps" result.txt'])
            #output_sco = subprocess.check_output(['grep "Score" result.txt'])
            try:
                simi = output_ide.strip().split('(')[1].split(')')[0].split('%')[0]
                res = output_ide.strip().split('\t')[1].split(' ')[0]
                total = int(simi) / 100
                print("similarity: ", total)
            except:
                res = "NA"
            line = str(key + "\t" + value + "\t" + str(res) + "\t" + str(total) + "\n")
            summary.write(line)
            subprocess.call(['rm','-f',fout])
            subprocess.call(['rm','-f',fout2])
            subprocess.call(['rm','-f',"./temp/result.txt"])
            print("###")
        else:
            count_na +=1
    summary.close()
    print(count_ort, count_na, c)


#species_list = ["Chimp","Bonobo","Gorilla","Orangutan","Rhesus","Atys","Baboon","GreenMonkey","MacFas","Marmoset","Lemur"]
#species_list = ["Chimp","Bonobo","Gorilla","Orangutan","Rhesus","Atys","Baboon","GreenMonkey","MacFas","Marmoset"]

species_list = ["Atys"]    
    
for x in species_list:
    print("current species ", x)
    align(x)










"""
primate = "Atys"
print(primate)
hum_primate_dict = {}
summary= open(str(primate + ".tsv"), 'w')

count_na = 0
count_ort = 0
c=0
with open('./human/counterparts.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        else:
            hum_primate_dict[row["lnc"]] = row[str(primate)]

for key, value in hum_primate_dict.items():
    if value != "NA":
        print(key, value)
        count_ort +=1
        fout = str('./temp/' + key + "_" + value + ".fa")
        fin = open('./human/transcripts.fa', 'r')
        f2in = open(str('./primates/done/' + primate +"/" + primate + ".fa"))
        pair = []
        for record in SeqIO.parse(fin,'fasta'):
                c+=1
                if record.id == key:
                    #print(record.format("fasta"))
                    seqLen = len(record)
                    #print(seqLen)
                    pair.append(record)
                else:
                    pass
        for record in SeqIO.parse(f2in,'fasta'):
                c+=1
                if record.id == value:
                    #print(record.format("fasta"))
                    pair.append(record)
                    SeqIO.write(pair, fout, "fasta")
                else:
                    pass
        fin.close()
        f2in.close()
        cmd = str("clustalo -i " +  fout + ' --outfmt=clu --residuenumber | fgrep -o "*" | wc -l')
        ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        res = int(ps.communicate()[0])
        total= ((int(res) * 100) / seqLen) / 100 
        print("similarity: ", total)
        line = str(key + "\t" + value + "\t" + str(res) + "\t" + str(total) + "\n")
        summary.write(line)
        subprocess.call(['rm','-f',fout])
        print("###")
    else:
        count_na +=1
summary.close()
print(count_ort, count_na, c)
            


"""
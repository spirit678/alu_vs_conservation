#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 10:07:08 2018

@author: spirit678
"""
import subprocess

f0=open("./lnc_vs_Alu.bed", "r")
out=open("./Alu.bed", "w")

for line in f0:
	c=0
	j=0
	line1 = line.strip().split('\t')
	chromosome=line1[12]
	start=str(line1[13])
	end=line1[14]
	Alu=line1[15]
	score=line1[16]
	strand=line1[17]
	new_line = chromosome + "\t" + start + "\t" + end + "\t" + Alu + "\t" + score + "\t" + strand + "\n"
	out.write(new_line)


        

out.close() 
f0.close()          

subprocess.call(["bedtools maskfasta -fi  ./hg38.fa -bed ./Alu.bed -fo ./human.fa"]) 

bedtools getfasta -name -split -fi ./human.fa -bed ./transcripts_all.bed -fo ./transcripts.fa


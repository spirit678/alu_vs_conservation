# -*- coding: utf-8 -*-
"""
Created on Wed Feb 15 17:38:46 2017

@author: spirit678
"""

import os
#import csv
import sys
chrom= {} 

f0=open("./genome/GreenMonkey_old.bed", "r")
f=open("./genome/chlS.txt", "r")

out=open("./genome/GreenMonkey.bed", "w")
dict_names={}

for line in f:
	if not line.startswith("#"):
		if line.startswith("chr"):
			#print(line)
			line = line.strip().split('\t')
			chromosome = line[0]
			scaff = line[6]
			#print(chromosome)
			#print(scaff)
			if scaff not in chrom:
		    		chrom[scaff] = chromosome
			else:
		    		pass
		elif line.startswith("MT"):
			line = line.strip().split('\t')
			chromosome = line[0]
			scaff = line[6]
			#print(scaff)
			if scaff not in chrom:
		    		chrom[scaff] = chromosome
			else:
		    		pass
		else:
			line = line.strip().split('\t')
			if line[9].find('chr') == 0:
				chromosome = line[9]
				scaff = line[6]
				#print(scaff)
				if scaff not in chrom:
		    			chrom[scaff] = chromosome
				else:
		    			pass


for line in f0:
	l1 = line.strip().split('\t')
	scaff2 = l1[0]
	print(scaff2)
	if scaff2 in chrom:
	    chromos = chrom.get(scaff2)
	    #print(scaff2)
	    print(chromos)            
	    line.replace(scaff2, chromos)
	    #print(line.replace(scaff2, chromos))           
	    text = line.replace(scaff2, chromos)
	    out.write(text)
	else:
	    out.write(line)

f0.close()
out.close()








